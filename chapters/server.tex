\settocdepth{section}

\chapter{Serverová část}

V této kapitole se zabývám realizací serverové části, čemuž předchází výběr technologií, návrh, analýza a tvorba podpůrné dokumentace ve formě diagramů. Serverová část zajišťuje správu a persistenci dat a navenek poskytuje rozhraní, s kterým komunikuje mobilní aplikace.

\section{Analýza a návrh}

V rámci analýzy je důležité ujasnit si princip fungování a spojitosti mezi jednotlivými prvky systému. Analýza a návrh dovolují odhalit potenciální problémy a nalézt jejich řešení.

\subsection{Výběr technologií}

Volba technologií je jedním z prvních a velmi důležitých kroků při tvorbě informačního systému. Pro každý projekt je nutné vyhodnotit jeho potřeby, na jejichž základě se vývojový tým rozhodne, jaký programovací jazyk, knihovny a další služby využít. Prioritou může být výpočetní rychlost, rychlost vývoje, stabilita nebo naopak nové experimentální funkcionality či velikost podpůrné komunity.

Pro výčet nejpopulárnějších jazyků jsem zvolil PYPL index (PopularitY of Programming Language)\cite{pypl}. Ten předpokládá fakt, že čím více je jazyk vyhledávaný, tím více je populární. Samotná data pocházejí z Google Trends\cite{google_trends} a~seřazena jsou podle podílu z celkové sumy vyhledávacích dotazů. Data z~dubna 2022 lze přehledně vidět v tabulce \ref{tab:pypl}. Sloupec trend udává rozdíl oproti dubnu 2021.

\begin{table}[h!]
\centering
\begin{tabular}{ |p{2cm}|p{5cm}|p{3cm}|p{2cm}| }
	\hline
	\textbf{Pořadí} & \textbf{Programovací jazyk} & \textbf{Podíl} & \textbf{Trend} \\
	\hline
	1. & Python & 27.95 \% & -2.2 \% \\
	\hline
	2. & Java & 18.09 \% & +0.6 \% \\
	\hline
	3. & JavaScript & 9.14 \% & +0.5 \% \\
	\hline
	4. & C\# & 7.39 \% & +0.5 \% \\
	\hline
	5. & C/C++ & 7.06 \% & +0.4 \% \\
	\hline
	6. & PHP & 5.48 \% & -0.9 \% \\
	\hline
	7. & R & 4.41 \% & +0.5 \% \\
	\hline
	8. & TypeScript & 2.27 \% & +0.5 \% \\
	\hline
	9. & Objective-C & 2.23 \% & -0.3 \% \\
	\hline
	10. & Swift & 2.06 \% & +0.2 \% \\
	\hline
\end{tabular}
\caption{PYPL Index (PopularitY of Programming Language), duben 2022, trend oproti dubnu 2021\cite{pypl}}
\label{tab:pypl}
\end{table}

V následujících sekcích uvedu, které technologie jsem zvolil a u hlavních i~důvody, které mě k volbě vedly. Velké množství technologií bylo zvoleno na základě pracovních zkušeností s nimi a jejich použití na projekt, který nevyžaduje experimentální funkcionality, je vhodné, neboť jsou praxí osvědčené.

\subsubsection{NodeJS (JavaScript / TypeScript)}

\uv{\emph{JavaScript (JS) je lightweight, interpretovaný nebo just-in-time kompilovaný programovací jazyk s first-class funkcemi. I když je nejznámější jako skriptovací jazyk pro webové stránky, je hojně využíván také v prostředí bez prohlížeče, jako například Node.js, Apache CouchDB a Adobe Acrobat. JavaScript je prototypový, multiparadigmatický, jednovláknový, dynamický jazyk, který podporuje objektově orientované, imperativní a deklarativní styly}}\cite{javascript} (přeloženo autorem)

TypeScript je následně nadstavbou JavaScriptu přinášející silné typování.

\uv{\emph{Node.js aplikace běží v jednom procesu bez vytváření nových vláken pro každý požadavek. Node.js poskytuje sadu asynchronních I/O operací ve své standardní knihovně, které zabraňují blokování JavaScript kódu a obecně, knihovny pro Node.js jsou psány s využitím neblokujících paradigmat, takže blokující chování je spíše výjimkou nežli normou.
\\\\
Provádí-li Node.js I/O operaci, jako například čtení ze sítě, přístup k databázi či souborovému systému, místo blokování vlákna a plýtvání CPU cykly čekáním Node.js pokračuje v operacích, až když dostane nazpět odpověď.}}\cite{nodejs} (přeloženo autorem)

Node.js je tedy asynchronní, událostmi řízené běhové prostředí pro JavaScript postavené na \emph{V8 JavaScript Engine}\cite{v8engine}, pomocí kterého lze velmi lehce vytvářet škálovatelné systémy, u kterých odpadá nutnost starat se o správu vláken a procesů. Důležitým pojmem je takzvaný \emph{event-loop}\cite{event_loop}, který stojí na pozadí asynchronního a neblokujícího chování.

\textbf{Důvody výběru:}

Vyvíjená aplikace nevyžaduje dlouhé a složité výpočty, naopak jednou z nejdůležitějších akcí je přístup do databáze. Asynchronní neblokující přístup je tedy velmi důležitý. Jednoduchost JavaScriptu a odpadající potíže se správou vláken/procesů či automatickou alokací a dealokací objektů jsou velmi vhodné argumenty pro vývoj, který není primárně zaměřený na výpočetní rychlost, a~pro projekt, který si klade za cíl ověření konceptu a vytvoření MVP (minimum viable product, minimální životaschopný produkt). Rozšířenost a popularitu těchto technologií dokazuje velikost komunity, což se může hodit pro další vývoj projektu.

\subsubsection{Yarn\cite{yarn}}

V rámci NodeJS se využívají dva hlavní správci balíčků a knihoven –~npm\cite{npm} a~Yarn. Při jejich používání nenalezneme příliš rozdílů, které se navíc časem stírají, avšak velmi často je upřednostňován Yarn díky lehce lepšímu výkonu\cite{npm_vs_yarn}. Není-li však potřeba se zabývat každou vteřinou, není volba pro běžný projekt důležitá. Přechod z jednoho na druhý je navíc velmi triviální. V rámci této práci využívám správce Yarn.

\subsubsection{NestJS\cite{nestjs}}
\label{chap:nestjs}

NestJS je pokročilý framework pro vývoj aplikací v TypeScriptu (potažmo JavaScriptu) postavený nad frameworkem express\cite{expressjs} (nabízí i možnost využít fastify\cite{fastify} místo express). Express nabízí minimální a tedy flexibilní kostru pro vývoj mobilních či webových aplikací. NestJS přidává složitější funkcionalitu, kterou popíši v následujících odstavcích.

\textbf{Modularizace}

Základní prvkem pro přehlednost je možnost modularizace kódu. Modul v~rámci frameworku NestJS obsahuje veškerý kód k obsloužení určité funkcionality. Zároveň však může mít závislost na některém z dalších modulů –~například pro správu uživatelů, práv, souborů, notifikací či mailů. NestJS tedy primárně uskupuje soubory podle úlohy a až sekundárně podle typu (myšleno controller, definice entit, rozhraní).

Ukázka:

\dirtree{%
	.1 /src.
	.2 config.
	.2 migrations.
	.2 shared.
	.2 scripts.
	.2 modules.
	.3 account.
	.4 account.module.ts.
	.4 services.
	.4 resolvers.
	.4 entities.
	.4 interfaces.
	.4 dto.
	.4 enums.
	.3 auth.
	.4 auth.module.ts.
	.4 services.
	.4 resolvers.
	.4 entities.
	.4 interfaces.
	.4 dto.
	.4 enums.
	.3 mail.
	.3 storage.
	.3 notification.
}

\textbf{Dependency injection}

Dependency injection je technikou, ve které vývojář deleguje vytváření instancí závislostí na takzvaný DI Container. Nemusí se o tuto část tedy starat sám manuálně. NestJS toto nabízí pomocí dekorátoru @Injectable() a následného zaregistrování služby v modulu. V rámci jednoho modulu mají služby přístup ke všem dalším, avšak napříč moduly je nutné, aby jeden službu exportoval a druhý naopak importoval. NestJS má i řešení, vznikne-li kruhová závislost.

\textbf{5 druhů middleware}

NestJS nabízí 5 druhů middleware, kde každý má určitý způsob využití:

\begin{itemize}
	\item \textbf{Middlewares} – funkce, které jsou volány před zpracováním požadavku konkrétním obsluhovačem v controlleru. Mají přístup k objektu žádosti i odpovědi.
	\item \textbf{Exception filters} – vrstva, která odchytává vyhozené výjimky v rámci zpracování požadavků.
	\item \textbf{Pipes} – jsou využívány k transformaci a validování příchozích dat.
	\item \textbf{Guards} – hlídají, zdali požadavek má být obsloužen, nejčastěji spojeno s kontrolou práv.
	\item \textbf{Interceptors} – se využívají k přidání logiky navíc před zpracováním a~po zpracování požadavku.	
\end{itemize}

Pozornému člověku neujde, že jisté typy úloh by šly odbavit pomocí vícero metod, jejich dělení je tak z velké části i pro přehlednost.

\textbf{Rozšíření}

V rámci ekosystému NestJS je velmi jednoduché začlenit složitější služby a~techniky –~například zprostředkovatele zpráv a událostí (Redis, Kafka, RabbitMQ, MQTT), dotazovací jazyk GraphQL, validace přicházejících dat, ukládání do mezipaměti, serializace či verzování endpointů.

\subsubsection{TypeORM\cite{typeorm}}

Pro manipulaci s daty jsem vybral TypeORM, nástroj pro automatickou konverzi mezi objekty v rámci kódu aplikace a daty v relační podobě uložené v~databázi. TypeORM nabízí rozvinutou funkcionalitu na práci s daty a relacemi mezi objekty, na vytváření migrací (změn v rámci schématu databáze) či na správu transakcí.

\subsubsection{GraphQL a Apollo GraphQL\cite{apollographql}}
\label{chap:server_apollo_graphql}

\textbf{GraphQL} je jazykem pro dotazování a manipulaci s daty. Data jsou vnímána jako propojený graf, ze kterého si dotazem vybíráme pouze určitou část. GraphQL, ačkoliv není softwarový architektonický styl, se považuje jako alternativa k RESTful rozhraní. Jednoduchý rozdíl lze pozorovat na obrázku\footnote{Pro vložení obrázku do diplomové práce převážila jeho výstižná popisnost nad neformalitou.}~\ref{fig:graphql_vs_rest}.

\begin{figure}[h!]
    \centering
	\includegraphics[width=1\textwidth]{images/graphql_vs_rest.png}
   	\caption{GraphQL vs Rest\cite{graphql_vs_rest}}
	\label{fig:graphql_vs_rest}	
\end{figure}

Základem serverové části GraphQL komunikace je jediný koncový bod, na který jsou směřovány veškeré dotazy. Ten na základě obsahu dotazu volá příslušné operace \emph{resolverů}, které konkrétní požadavek obslouží. Samotná implementace získávání a zpracování dat je čistě na programátorovi, a tím se může lišit projekt od projektu. Jelikož data jsou vnímaná jako graf, dotazující má možnost se neomezeně zanořovat, je-li k tomu server uzpůsoben. K~tomu v~rámci GraphQL jsou \emph{field resolvers}, které slouží právě k získávání dat zanořených v existující entitě. Příklad uvádím na zdrojovém kódu \ref{code:apollo_graphql_in_nestjs}.

\textbf{Apollo GraphQL} je sadou nástrojů pro usnadnění vývoje jak na straně klienta, tak i serveru. Přináší možnosti automatického generování GraphQL schémat na backendové straně a jednoduše je přenášet do webových i mobilních aplikací. Zároveň existuje balíček na integraci s NestJS vyvíjený přímo pod hlavičkou NestJS.

\lstjs
\begin{lstlisting}[caption={Ukázka Apollo GraphQL v NestJS}, captionpos=b, label={code:apollo_graphql_in_nestjs}]
@Resolver(of => Author)
export class AuthorsResolver {
  constructor(
    private authorsService: AuthorsService,
    private postsService: PostsService,
  ) {}

  @Query(returns => Author)
  async author(@Args('id', { type: () => Int }) id: number) {
    return this.authorsService.findOneById(id);
  }

  @ResolveField()
  async posts(@Parent() author: Author) {
    const { id } = author;
    return this.postsService.findAll({ authorId: id });
  }
}
\end{lstlisting}

\subsubsection{PostgreSQL}

Aplikace počítá s ukládáním dat v strukturované podobě, která je předem známá. Zároveň se předpokládá větší důraz na čtení dat před jinými operacemi. Taktéž není potřeba brát ohled na souběžný zápis, ke kterému nebude docházet. Volba tedy byla namířena na relační databáze –~konkrétně na nejznámější PostgreSQL a MySQL. Finální rozhodnutí bylo ovlivněno hostingem, kde bude celý systém testován a prvotně spuštěn. V rámci něho je nejvýhodnější možnost použití PostgreSQL databáze. Jelikož aplikace nevyžaduje funkcionalitu specifickou pro určitý databázový stroj, byla dána přednost tomuto systému. TypeORM má podporu obou strojů a případný přechod by znamenal vygenerování nových migrací a přenos dat.

\subsubsection{Další knihovny a balíčky}

\textbf{Firebase}\cite{firebase_cloud_messaing}

Pro zasílání notifikací byla využita služba Firebase. Ta kromě notifikací nabízí služby na sledování statistik, přihlašování přes vícero externích služeb či vlastní databázové řešení. Avšak žádná z dalších možností není v systému ke dni odevzdání práce využita.

\textbf{AWS S3}\cite{aws_s3}

Na ukládání fyzických souborů jako například profilová fotografie či přílohy v~rámci funkce Trip je zvolena cloudová služba S3 od Amazon Web Services.

\subsection{Návrh architektury}

Architektura serverové části je pro větší přehlednost a udržitelnost rozdělena na tři vrstvy. Diagram lze vidět na obrázku \ref{fig:architecture}.

\subsubsection{Prezentační vrstva}

Prezentační vrstva v rámci této práce obsahuje pouze jeden balíček tříd s~názvem \emph{Resolver}. Třídy v rámci tohoto balíčku odbavují požadavky GraphQL komunikace, kde jednotlivé funkce se mapují na konkrétní dotazy a mutace. V~deklaraci jsou využity objekty z balíčku \emph{Dto} (data transfer object) z business vrstvy, které obsahují specifikaci přijímaných dat a validačních pravidel. Tyto celé objekty jsou předávány ve volání specifických metod z business vrstvy a~výsledek se následně vrací jako odpověď na požadavek.

\subsubsection{Business vrstva}

Business vrstva je, co se do počtu balíčků týče, nejobsáhlejší a implementuje logiku zpracování dat a kontroly přístupu.

Hlavní balíček má název \emph{Service} a obsahuje třídy, jenž jsou zodpovědné právě za implementaci business logiky. Reagují na podněty z prezentační vrstvy, zpracovávají data, volají metody na kontrolu práv, v případě problémů vyhazují výjimky a finálně volají funkce datové vrstvy na manipulaci s~daty v~databázi.

\emph{Voter} skýtá třídy definující pravidla, kdo a za jakých podmínek může provádět určité akce a manipulovat s daty.

\emph{Dto} obsahuje objekty používané k přenosu dat –~dto je zkratkou pro data transfer object (objekty pro přenos dat). Ty se využívají již při externí komunikaci prezentační vrstvy a klientské aplikace. Kromě definice vlastností slouží i~k~definování jednoduchých validačních pravidel (délka řetězce, povinnost vyplnění) pomocí dekorátorů. O samotnou validaci se pak stará validační \emph{pipa} popsaná v kapitole \ref{chap:nestjs} týkající se frameworku NestJS.

Balíček \emph{Notification} obsahuje pure funkce (čisté funkce) na generování těl notifikací. Takto pospolu je následně jednoduché dělat potenciální úpravy struktury upozornění.

\emph{Exception} obsahuje obdobně jako \emph{Notification} pouze pure funkce na generování výjimek, aby byly lehce přepoužitelné a v případě potřeby změny, aby veškeré definice byly na jednom místě.

\subsubsection{Datová vrstva}

Datová vrstva obsahuje definice entit v balíčku \emph{Entity}, rozhraní entit v \emph{Interface} a enumerace, například pro stavy či typy, v balíčku \emph{Enum}. Vyčlenění rozhraní je z toho důvodu, že v samotných třídách entit jsou i dekorátory pro GraphQL komunikaci a definice pro uložení do databáze, a soubory tak narůstají na velikosti.

\clearpage
\vfill
\begin{figure}
    \centering
	\includegraphics[width=1\textwidth]{images/architecture.png}
   	\caption{Návrh architektury}
	\label{fig:architecture}
\end{figure}
\vfill
\clearpage

\subsection{Diagram entitních tříd}

Na obrázku \ref{fig:safie_classes} lze vidět diagram entitních tříd (barevně odlišeny jednotlivé funkcionality). Základní entitou je \emph{Account} reprezentující uživatelský účet, u~kterého se evidují informace o emailu, uživatelském jméně, identifikátory na propojení se sociálními sítěmi či profilový obrázek.

Na vytvoření nouzových kontaktů slouží entita \emph{Guardian}. \emph{AccountDeviceToken} obsahuje údaj o identifikátoru fyzického zařízení pro posílání notifikací. \emph{File} je entitou reprezentující soubor.

Pro funkcionalitu \textbf{SOS} tlačítka existují dvě entity s názvy \emph{Sos} a \emph{SosPosition}. První reprezentuje samotné stisknutí a stav, ve kterém se nachází. Slouží pro uchování informací o čase zmáčknutí, puštění, zadání PIN kódu či upozornění nouzových kontaktů. Druhá entita zastupuje informaci o uživatelově poloze v~čase. 

\textbf{Checkin} je reprezentován třemi entitami. \emph{CheckinRequest} představuje samotné vytvoření požadavku na přihlášení a obsahuje informaci o typu (jednorázový, pravidelný) a pro pravidelný i údaje o začátku, konci a intervalu. \emph{CheckinRequestGuardian} je vazbou mezi požadavkem a uživatelským účtem a~funguje obdobně jako entita \emph{Guardian}, reprezentuje tedy ochránce, kteří jsou informování ohledně stavu checkinu. Finální entitou je \emph{Checkin}, jenž je záznamem ohledně jednoho konkrétního požadavku na přihlášení a skýtá informaci o stavu, zdali uživatel zadal, že je v pořádku a případně i lokaci, ve které požadavek potvrdil.

Pro funkcionalitu týkající se cest (\textbf{Trip}) jsou nadefinovány čtyři entity. \emph{Trip} zastupující jednu cestu a obsahující informaci o předpokládaném čase příchodu, cílovou destinaci a předběžný plán cesty. Pro práci s místy je k dispozici entita \emph{TripDestination} a pro jednotlivý krok plánu cesty slouží entita \emph{TripQuickAction}. Dílčí aktivita finální cesty je reprezentována entitou \emph{TripActivity}, která obsahuje informaci o typu, poloze, stavu baterie a následně dodatečné informace specifické pro jednotlivé typy aktivit.

\clearpage
\vfill
\begin{figure}
    \centering
	\includegraphics[width=1\textwidth]{images/safie_classes.png}
   	\caption{Diagram entitních tříd}
	\label{fig:safie_classes}
\end{figure}
\vfill
\clearpage

\section{Realizace}

V této kapitole rozebírám, jakým stylem je finální implementovaná serverová část rozdělena a jakým způsobem jsou realizovány důležité části systému. Není zde kompletní popis či dokumentace API rozhraní, neboť pomocí nástrojů Apollo GraphQL se striktně typované schéma samo propaguje napříč platformami.

\subsection{Moduly}

Serverová část se skládá z celkem 12 modulů, z nichž 2 nejsou aktuálně využívané (modul na odesílání mailů a modul na správu událostí –~návrhový vzor publisher-subscriber). Zbylých deset vykonává následující funkce:

\begin{itemize}
	\item \textbf{Account} – správa uživatelů,
	\item \textbf{Auth} – autentizace/autorizace,
	\item \textbf{Checkin} – funkcionalita ohledně checkinů,
	\item \textbf{Firebase} – napojení na Firebase knihovnu,
	\item \textbf{Guardian} – funkcionalita ohledně nouzových kontaktů,
	\item \textbf{Notification} – odesílání notifikací,
	\item \textbf{Sos} – funkcionalita ohledně kritických událostí,
	\item \textbf{Storage} – správa souborů, komunikace s AWS S3,
	\item \textbf{Trip} – funkcionalita ohledně cest,
	\item \textbf{Voter} – kontrola přístupu.
\end{itemize}

\subsection{Konfigurace}

Pro nakonfigurování určitých částí systému je vytvořena třída \emph{ConfigService}, jež obsahuje veřejné metody, které vracejí objekty s příslušnými parametry. Je využito proměnných prostředí (environment variables), které jsou v některých případech nepovinné a mají tedy nadefinovanou výchozí hodnotu. Ukázka je dostupná na zdrojovém kódu \ref{code:config_service}.

\lstjs
\begin{lstlisting}[caption={Ukázka ConfigService}, captionpos=b, label={code:config_service}]
class ConfigService {
  constructor(private env: { [k: string]: string |\textbar{}| undefined }) {}
  
  private getValue(key: string, throwOnMissing = true): string {
    const value = this.env[key];

    if (!value && throwOnMissing) {
      throw new Error(`config error - missing env.|\$|{key}`);
    }

    return value;
  }
  
  public getS3() {
    return {
      accessKeyId: this.getValue('S3_ACCESS_KEY_ID', true),
      secretAccessKey: this.getValue('S3_SECRET_ACCESS_KEY', true),
      region: this.getValue('S3_REGION', true),
      bucket: this.getValue('S3_BUCKET', true),
      version: this.getValue('S3_VERSION', true),
      prefix: this.getValue('S3_PREFIX', false) |\textbar{}\textbar{}| '',
    };
  }
}

const configService = new ConfigService(process.env).ensureValues(['MODE', 'JWT_SECRET', 'DATABASE_URL']);

export { configService };
\end{lstlisting}

\subsection{Komunikace}

Jak již bylo zmíněno, komunikace probíhá pomocí dotazovacího jazyku GraphQL. V NestJS toho lze docílit nainstalováním balíčku \emph{@nestjs/graphql} a~následným zaregistrováním tohoto externího modulu pomocí naimportování do systému, nastavení určitých parametrů a vložením dekorátorů k příslušným metodám, které mají požadavky odbavovat.

\subsubsection{Autentizace a přihlášení přes externí služby}
\label{chap:authentication_sign_in}

Přihlášení přes všechny tři externí služby (Apple, Google, Facebook) bylo řešeno obdobným způsobem. Mobilní aplikace prezentuje uživateli přihlašovací okno, a pokud je výsledek úspěšný, pošle na server unikátní token/kód vygenerovaný externí službou, který je následně využit k získání údajů dalším dotazem, již však na straně backendu. V odpovědi tohoto požadavku jsou důležité informace jako email či identifikátor uživatele. Na serveru se vytvoří entita reprezentující uživatelův účet, uloží se do databáze a jako odpověď mobilní aplikaci se odešlou tokeny, jež se využívají v následující komunikaci pro autorizaci.

Pro dotazy na Apple a Google jsou využity přímo knihovny těchto poskytovatelů a v případě služby Facebook je získání údajů řešeno dotazem na konkrétní endpoint. 

Další z možností, jak řešit autentizaci, by bylo využití jedné z autentizačních autorit, například Firebase Authentication\cite{firebase_auth}. Ty se starají o celý proces registrace, neboť nabízí komponenty do klientských částí i následné knihovny pro komunikaci v rámci backendu. Většinou nabízejí podporu pro velké množství přihlašovacích služeb, a tudíž přidání další možnosti je velmi jednoduché. Uživatelská data jsou v tomto případě však uložena na jejich serverech a vzniká zde závislost v kritické části aplikace, což je hlavním důvodem, proč toto řešení nebylo zvoleno.

Ačkoliv je po technické stránce vše připravené, v době odevzdávání této práce není přihlášení přes Google a Facebook v provozu, neboť je potřeba nejdříve vyřešit verifikaci účtu a dodání dokumentů jako například zpracování osobních údajů.

Diagram průběhu přihlášení lze vidět na obrázku \ref{fig:apple_sign_in_flow}.

\begin{figure}
    \centering
	\includegraphics[width=1\textwidth]{images/apple_sign_in_flow.png}
   	\caption{Průběh přihlášení přes Apple}
	\label{fig:apple_sign_in_flow}
\end{figure}

\subsection{Autorizace}

Po přihlášení a získání autorizačních tokenů následně mobilní aplikace první z nich (apiToken) odesílá v hlavičce každého požadavku. Server tuto hlavičku zpracuje a příslušné metodě dodá i konkrétní entitu uživatele, jež je následně využita pro kontrolu práv. Token má určitou dobu životnosti čítající několik jednotek hodin. V případě vypršení si musí aplikace vyžádat obnovení tokenů pomocí druhého (refreshToken). Druhý token má taktéž pevně nastavenou dobu platnosti, která je ovšem násobně delší (v desítkách dnů). Pokud není ani refreshToken platný, uživatel je vyzván, aby se znovu přihlásil.

\subsection{Kontrola práv – Voters}

O kontrolu práv, jak již bylo zmíněno, se stará modul Voter. Celý princip spočívá v globálně dostupné službě s názvem VoterService, která nabízí dvě veřejné metody. První funguje na zaregistrování voterů (hlasujících, obsluhovačů). Ty jsou implementovány v konkrétních modulech a starají se o~rozhodování nad právy týkajících se daného modulu. V rámci modulu Voter je definováno rozhraní, které musejí jednotlivé votery implementovat. Hlavní VoterService má následně uloženo pole voterů, které při dotazu prochází. Samotný voter nejdříve implementuje jednoduchou funkci, která rozhodne, jestli může dotaz obsloužit a poté ho případně obslouží. Ukázku voteru lze pozorovat na zdrojovém kódu \ref{code:guardian_voter} a VoterService na zdrojovém kódu \ref{code:voter_service}.

\lstjs
\begin{lstlisting}[caption={Ukázka GuardianVoter}, captionpos=b, label={code:guardian_voter}]
export class GuardianVoter extends IVoter {
  async supports(attribute: string): Promise<boolean> {
    return (<any>Object).values(GuardianAttributes).includes(attribute);
  }

  async voteOnAttribute(attribute: string, subject: any, account: IAccount): Promise<boolean> {
    switch (attribute) {
      case GuardianAttributes.REMOVE_GUARDIAN:
        return this.removeGuardian(account, subject);
      case GuardianAttributes.ACCEPT_GUARDIAN:
        return this.acceptGuardian(account, subject);
    }

    return false;
  }

  async removeGuardian(account: IAccount, guardian: IGuardian): Promise<boolean> {
    return account.id === guardian.account.id;
  }

  async acceptGuardian(account: IAccount, guardian: IGuardian): Promise<boolean> {
    return account.id === guardian.guardian.id;
  }
}
\end{lstlisting}

\lstjs
\begin{lstlisting}[caption={Ukázka VoterService}, captionpos=b, label={code:voter_service}]
@Injectable()
export class VoterService {
  private voters: IVoter[] = [];

  async isGranted(attribute: string, subject: any, account: IAccount): Promise<boolean> {
    for (const voter of this.voters) {
      if (await voter.isGranted(attribute, subject, account)) {
        return true;
      }
    }

    return false;
  }

  register(voter: IVoter): void {
    this.voters.push(voter);
  }
}
\end{lstlisting}

\subsection{Nastavení externích služeb}

V rámci celého systému se užívá řada externích služeb, které bylo potřeba nakonfigurovat a připravit pro použití. Jak již bylo zmíněno v kapitole \ref{chap:authentication_sign_in} Autentizace a přihlášení přes externí služby, je rozdíl mezi přípravou služeb pro testování a pro produkční použití, neboť mnohdy je potřeba náročnějšího nastavení a verifikace, obvzláště jedná-li se o manipulaci s daty.

Příprava Firebase a Amazon web services S3 proběhla velmi hladce a zde jsem založil účty přes svojí fyzickou osobu. U přihlášení přes sociální sítě (tedy přes Google a Facebook) nastal problém při přechodu do ostrého provozu, kdy je potřeba účet verifikovat a dodat podmínky použití a z tohoto důvodu se nepodařilo do odevzdání diplomové práce služby zpřístupnit široké veřejnosti.

S přihlášením přes účet Apple nebyl žádný problém, neboť vývojem právě pro platformu od Apple a s předem definovaným cílem nasazení pro běžné uživatele nebylo tyto věci nutno řešit specificky pro přihlášení. Samotný proces nasazení je popsán později v kapitole týkající se mobilní aplikace, konkrétně v sekci \ref{chap:testing_and_releasing}~Technická příprava testování a nasazení.

\subsection{Klíčová funkcionalita}

V následujících několik sekcích popíši, jakým způsobem byla implementována logika klíčové funkcionality, jaké problémy během implementace nastaly a způsoby, kterými byly vyřešeny.

\subsubsection{Nouzové kontakty}

Přidávání nouzového kontaktu funguje přes vyplnění uživatelského jména, kdy druhá osoba musí následně požadavek na přidání odsouhlasit. Na správu nouzových kontaktů je připraveno 5 různých koncových bodů:

\begin{itemize}
	\item \emph{getMyGuardians} – vypsání mých nouzových kontaktů,
	\item \emph{getGuardianRequests} – vypsání požadavků na přidání nouzových kontaktů,
	\item \emph{addGuardian} – poslání požadavku na přidání,
	\item \emph{acceptGuardian} – přijetí požadavku na přidání,
	\item \emph{removeGuardian} – odstranění nouzového kontaktu.
\end{itemize}

Nejdříve bylo implementováno pouze jednosměrné přidání kontaktů –~tedy přidal-li jsem si já nouzový kontakt, nebyl jsem automaticky já jeho ochráncem. Avšak v~kódu i logice zpracování následně docházelo k velkému zmatku, a~tak nakonec toto chování bylo změněno na obousměrné –~při přijetí požadavku se tak vytvoří druhá entita s prohozeným pořadím.

\subsubsection{SOS tlačítko}

Princip, jenž je z části vidět na stavovém diagramu na obrázku \ref{fig:sos_states}, této funkcionality spočívá v tom, že uživatel zmáčkne tlačítko a během jeho držení aplikace odesílá pravidelně informaci o uživatelově poloze. Následně po puštění tlačítka uživatel buď do určitého časového horizontu zadá svůj PIN, nebo server odešle notifikaci nouzovým kontaktům. Pro obsloužení jsou vytvořeny čtyři koncové body:

\begin{itemize}
	\item \emph{sosButtonPressed} – uživatel stiskl tlačítko,
	\item \emph{sosButtonReleased} – uživatel pustil tlačítko,
	\item \emph{sosPinEntered} – uživatel zadal svůj PIN,
	\item \emph{sosPositionUpdate} – aktualizace polohy během držení.
\end{itemize}

Pro zobrazení historie těchto mimořádných událostí jsou definovány tyto endpointy:

\begin{itemize}
	\item \emph{getMySosHistory} – výpis mých nouzových situací,
	\item \emph{getSosesOfAccountsIAmGuardianOf} – výpis nouzových situací mých kontaktů,
	\item \emph{getSosPositions} – výpis zaznamenaných poloh během držení tlačítka.
\end{itemize}

\begin{figure}
    \centering
	\includegraphics[width=1\textwidth]{images/sos_states.png}
   	\caption{Stavový diagram SOS}
	\label{fig:sos_states}
\end{figure}

\subsubsection{Checkin}

Pro manipulaci s checkinem jsou vytvořeny celkem čtyři koncové body. Dva na vyžádání rozdělené podle typu, jeden na zrušení pravidelného a jeden na potvrzení.

\begin{itemize}
	\item \emph{requestCheckin} – vyžádání jednorázového přihlášení,
	\item \emph{requestPeriodicalCheckin} – vyžádání pravidelného přihlášení,
	\item \emph{cancelPeriodicalRequest} – zrušení probíhajícího pravidelného checkinu,
	\item \emph{checkin} – potvrzení konkrétního checkinu.
\end{itemize}

Samotný požadavek na checkin vytváří pouze záznam v databázi s příslušnými informacemi a o poslání požadavku konkrétnímu uživateli se stará až naplánovaná úloha, která se spouští každých 10 vteřin. Entita \emph{CheckinRequest} má atribut udávající, kdy má být vyslán další požadavek (\emph{nextCheckinAt}). Tato naplánovaná úloha tedy odbavuje záznamy, které mají tuto hodnotu v minulosti. V případě pravidelného checkinu rovnou uloží, kdy má dojít k dalšímu.

\lstjs
\begin{lstlisting}[caption={Ukázka zpracování požadavku na pravidelný checkin}, captionpos=b, label={code:resolver_periodical_checkin}]
@Mutation(() => Boolean)
async requestPeriodicalCheckin(
  @GqlAccount() account: IAccount,
  @Args('periodicalCheckinRequest') checkinRequestPeriodicalDto: CheckinRequestPeriodicalDto,
) {
  await this.checkinService.requestPeriodicalCheckin({ account, checkinRequestPeriodicalDto });

  return true;
}
\end{lstlisting}

\clearpage
\lstjs
\begin{lstlisting}[caption={Ukázka DTO pravidelného checkinu}, captionpos=b, label={code:dto_periodical_checkin}]
@InputType()
export class CheckinRequestPeriodicalDto {
  @Field(() => ID)
  @IsNotEmpty()
  readonly targetAccountId: number;

  @Field()
  @IsNotEmpty()
  readonly startsAt: Date;

  @Field()
  @IsNotEmpty()
  readonly endsAt: Date;

  @Field()
  @IsNotEmpty()
  readonly periodInMinutes: number;

  @Field(() => [ID])
  @IsNotEmpty()
  readonly guardianAccountIds: number[];
}
\end{lstlisting}

\lstjs
\begin{lstlisting}[caption={Ukázka plánované úlohy}, captionpos=b, label={code:cron_checkin}]
@Cron(CronExpression.EVERY_10_SECONDS)
async createCheckins(): Promise<void> {
  const requests = await this.checkinRequestRepository.find({
    relations: ['targetAccount', 'requester'],
    where: {
      nextCheckinAt: LessThan(new Date()),
    },
  });

  for (const request of requests) {
    await this.createCheckin({ request });
  }
}
\end{lstlisting}

Kromě manipulačních koncových bodů je vytvořeno 5 dalších na výpis a získávání informací:

\begin{itemize}
	\item \emph{getRequestsForMe} – výpis požadavků, kde jsem já byl požádán o přihlášení,
	\item \emph{getRequestsIFollow} – výpis požadavků, ve kterých figuruji jako kontakt,
	\item \emph{getRequest} – detail o konkrétním požadavku,
	\item \emph{getCheckinsByRequest} – výpis jednotlivých dílčích požadavků o přihlášení (vztah 1:N u \emph{CheckinRequest} a \emph{Checkin}),
	\item \emph{getUncheckedCheckinsForMe} – výpis požadavků, které jsem ještě nepotvrdil.
\end{itemize}

\subsubsection{Trip}

Finální popisovanou funkcionalitou, avšak pravděpodobně nejsložitější, je zaznamenávání průběhu cesty. Princip spočívá v zahájení cesty, kdy uživatel může zvolit čas příchodu a cílovou destinaci. Zároveň si může naplánovat průběh cesty pomocí rychlých akcí, kterými jsou například čekání, jízda tramvají, přestup či chůze pěšky. Po zahájení může volit z těchto přednastavených rychlých akcí, případně vybírat ze zbylých a také může přidávat zprávy a fotky. Pro vytváření tohoto průběhu cesty existuje 5 manipulačních koncových bodů:

\begin{itemize}
	\item \emph{createTrip} – vytvoření cesty,
	\item \emph{endTrip} – ukončení cesty,
	\item \emph{addMessageToTrip} – přidání zprávy k cestě,
	\item \emph{addPhotoToTrip} – přidání fotky k cestě,
	\item \emph{addQuickActionToTrip} – přidání rychlé akce k cestě.
\end{itemize}

V požadavcích na poslední tři koncové body jsou taktéž přítomny informace o uživatelově poloze a stavu baterie jeho zařízení. Pro zobrazení historie cest existuje následujících 5 endpointů:

\begin{itemize}
	\item \emph{getActiveTrip} – získání aktuálně aktivní cesty (je-li nějaká),
	\item \emph{getMyTrips} – historie cest,
	\item \emph{getTrip} – detail konkrétní cesty,
	\item \emph{getMyGuardiansTrips} – historie cest mých kontaktů,
	\item \emph{getActivitiesByTrip} – výpis aktivit konkrétní cesty.
\end{itemize}

V rámci této funkcionality existují ještě 2 finální koncové body:

\begin{itemize}
	\item \emph{getMyTripDestinations} – výpis historie cílových destinací,
	\item \emph{toggleFavouriteTripDestination} – změna stavu oblíbení destinace.
\end{itemize}

Pro uživatele neexistuje komplexnější funkcionalita na správu destinací. Koncový bod na zahájení cesty přijímá buď identifikátor existující destinace, či objekt na vytvoření nové. Mazání není aktuálně dostupné, neboť výpis funguje jako historie.