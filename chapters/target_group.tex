\settocdepth{section}

\section{Cílová skupina}
\label{chap:target_group}

V rámci této sekce se kouknu na možnosti provádění výzkumu ohledně chování a potřeb lidí a potenciálních uživatelů, poté zvolím vhodnou variantu a sám jeden z výzkumů provedu.

Metody výzkumu lze rozdělit na dvě základní – kvantitativní a kvalitativní.

\uv{\emph{\textbf{Kvantitativní výzkum} se také označuje jako tradiční, pozitivistický, experimentální nebo empiricko-analytický. Zaměřuje se na hledání vztahů mezi dvěma či více proměnnými. Jeho hlavním cílem je ověřování platnosti teorií pomocí testování z těchto teorií vyvozených hypotéz.\\\\Kvantitativní výzkumník chápe sociální realitu jako existující nezávisle na jeho osobnosti. Udržuje si od ní odstup nezúčastněného pozorovatele a snaží se o~její popis. Lidské chování považuje za determinované a tedy měřitelné a předpověditelné.}}\cite{kvantitativni_vs_kvalitativni}

\uv{\emph{\textbf{Kvalitativní výzkum} bývá také nazýván konstruktivistickým, naturalistickým, interpretativním nebo reflexivním. Disman jej charakterizuje jako nenumerické šetření a interpretaci sociální reality. Kvalitativní přístup klade důraz na důkladné (hloubkové) poznání zkoumaného sociálního jevu (události, fenoménu). Snaží se o vytvoření komplexního, holistického obrazu zkoumaného problému, o porozumění lidem v různých sociálních situacích a jejich interpretacím těchto situací.\\\\Kvalitativní výzkumník se domnívá, že sociální realita by měla být spíše interpretována než popisována, a to na základě toho, jak ji prožívají konkrétní jednotlivci. Lidské chování je nahlíženo jako výsledek svobodných rozhodnutí.}}\cite{kvantitativni_vs_kvalitativni}

Rozdíly ve sběru dat obou metod jsou přehledně vidět v tabulce \ref{tab:kvantitativni_vs_kvalitativni}:

\begin{table}[h!]
\centering
\begin{tabular}{ |p{6cm}|p{6cm}| }
	\hline
	\textbf{Kvantitativní sběr dat} & \textbf{Kvalitativní sběr dat} \\
	\hline
	Vzorek respondentů je velký & Stačí málo respondentů \\
	\hline
	Provádí se pomocí dotazníkových šetření & Provádí se pomocí osobních rozhovorů \\
	\hline
	Snaží se kvantifikovat výskyt problému & Zkoumá problémy do hloubky \\
	\hline
	Časově nenáročný & Časově náročný \\
	\hline
	Dedukce z výsledků & Indukce z výsledků \\
	\hline
	Statistické zpracování dat & Nestatistické zpracování dat \\
	\hline
\end{tabular}
\caption{Kvantitativní vs kvalitativní sběr dat\cite{kvantitativni_vs_kvalitativni_survio}}
\label{tab:kvantitativni_vs_kvalitativni}
\end{table}

V této diplomové práci jsem se rozhodl osobně provést pouze kvalitativní výzkum, neboť vede k hlubšímu porozumění uživatelových potřeb a zároveň kvantitativní výzkumy na toto téma jsou dobře dohledatelné a byla jich provedena již celá řada. Následující podsekce \ref{chap:kvantitativni} a \ref{chap:kvalitativni} obsahují shrnutí získaných informací.

\subsection{Kvantitativní výzkum}
\label{chap:kvantitativni}

V kvantitativním výzkumu by měla být zodpovězena základní otázka, zdali se opravdu lidé v určitých situacích cítí nekomfortně. K jejich nalezení jsem použil vyhledávač Google\cite{google} a klíčovou frázi \uv{\emph{do women feel safe walking home alone at night}}. Z dostupných článků a prací na první stránce jsem nakonec zvolil výzkum z roku 2021 od britské společnosti YouGov\cite{yougov_poll_2021}, jenž byl dokonce citován i na dalších odkazech. YouGov je mezinárodní skupinou provádějící sběr a následnou analýzu dat a nejcitovanějším zdrojem v oblasti výzkumu trhu ve Spojeném Království.\cite{yougov_about}

Základní informace o provedeném výzkumu shrnuji v tabulce \ref{tab:yougov_poll_info}.

\begin{table}[h!]
\centering
\begin{tabular}{ |p{6cm}|p{6cm}| }
	\hline
	Velikost vzorku & 1667 \\
	\hline
	Ženy & 857 \\
	\hline
	Muži & 810 \\
	\hline
	Region výzkumu & Velká Británie \\
	\hline	
\end{tabular}
\caption{Základní informace o výzkumu ohledně pocitu nebezpečí společnosti YouGov\cite{yougov_poll_2021}}
\label{tab:yougov_poll_info}
\end{table}

V rámci tohoto výzkumu bylo pokládáno několik otázek a tázaní odpovídali, jak často se v daných situacích necítí bezpečně. Tabulka \ref{tab:yougov_poll} obsahuje procentuální zastoupení lidí, kteří odpověděli \uv{vždy} nebo \uv{často}. Důležitou informací je fakt, že 19 \% dotázaných žen vůbec v noci o samotě nechodí.

\begin{table}[h!]
\centering
\begin{tabular}{ |p{6cm}|p{3cm}|p{3cm}| }
	\hline
	Situace & Ženy & Muži \\
	\hline
	Chození v noci o samotě & 63 & 15 \\
	\hline
	Chození alejí, uličkou, průchodem (anglické \uv{alley}) o samotě & 63 & 17 \\
	\hline
	První schůzka, rande (anglické \uv{date}) & 28 & 4 \\
	\hline
	Návštěva domova cizího člověka & 31 & 9 \\
	\hline
\end{tabular}
\caption{Procento lidí, kteří se v daných situacích necítí bezpečně (\uv{vždy} nebo \uv{často})\cite{yougov_poll_2021}}
\label{tab:yougov_poll}
\end{table}

\subsection{Kvalitativní výzkum}
\label{chap:kvalitativni}

Kvalitativní výzkum jsem v rámci této práce provedl osobně. Vytipoval jsem si tři osoby ve svém blízkém okolí, které mají zkušenosti s chozením domů v~noci o samotě a provedl s nimi 30-60 minutový rozhovor. Konverzace neměla striktní strukturu, nýbrž přirozeně plynoucí atmosféru. Cílem rozhovorů bylo zjistit následující body:

\begin{itemize}
	\item obecný pohled na toto téma,
	\item způsob, jakým se v určitých situacích samy chovají,
	\item jaké prostředky využívají pro informování ostatních (zdali vůbec nějaké),
	\item co by jim usnadnilo jejich situace či nějakým způsobem pomohlo.
\end{itemize}

Finálně jsem také vyhledal zkušenosti lidí na internetu.

\subsubsection{Rozhovory}

Rozhovory proběhly se třemi respondentkami ve věku od 19 do 26 let.

Všechny tři respondentky se shodly, že při cestě o samotě pociťují strach, a tudíž podnikají určité kroky – převážně kontaktují své blízké, že na cestu vyrážejí a během ní o sobě dávají občas vědět. Problém vidí v situacích, kdy nikdo z nejbližších není dostupný – například již všichni spí. V tuto chvíli je informování o své poloze a situaci minimální. Dvě z dotázaných navíc s nikým během cesty nesdílí svojí polohu pomocí dalších aplikací. Pokud by nastala kritická situace, není zde žádná přibližující informace na jaké části trasy se daná věc udála. Všem třem se tedy líbil nápad ohledně možnosti u každé zprávy mít informaci o poloze, ze které byla odeslána.

Jedna z respondentek sama zmínila určitou nevhodnost kontaktování přes klasické aplikace pro psaní zpráv. Je-li v roli toho, který je doma a strachuje se o~druhého, zdráhá se mu v této situaci psát – druhá strana může být naprosto v pořádku a bavit se a považovat zprávu za otravnou. Zároveň vyslovila myšlenku, že někteří lidé automaticky notifikace z těchto aplikací ignorují nebo jim nepřidávají takovou váhu, neboť jim jich chodí velké množství. Myslí si tedy, že pokud by přišlo upozornění z aplikace dedikované na bezpečnost, kterou by druhý pouze rychle odkliknul, a dal tak o svém bezpečí vědět, měli by lidé menší tendenci je přecházet. I pro ni samotnou by to znamenalo, že se necítí \uv{\emph{vlezle}}. Zajímavou zmínkou respondentky taktéž bylo, že by notifikace z aplikace vyhraněné k tomuto tématu ji samotnou podnítila k tomu, aby se kolem sebe rozhlédla a zanalyzovala situaci – například při jízdě v tramvaji.

Další z respondentek vyprávěla příběh o rodinné kamarádce, jež byla napadena na zastávce autobusu, kde následně v bezvědomí ležela, dokud nepřivolal pomoc kolemjdoucí starší pár. Nešel-li by tento pár okolo, bůh ví jak dlouho by na místě byla, pokud by již nebylo pozdě. V tento moment by bylo důležité, aby někdo věděl informace o její lokaci a že se do nějaké doby neozvala. Jelikož se jednalo o již dospělou ženu, nikdo ji přímo nekontroloval a nečekal, zdali dorazí v pořádku domů. Například pouhé odkliknutí každých 10-30 minut by mohlo následně vyvolat u někoho z blízkých další kroky ke zjištění stavu.

Základní činnosti, které samy všechny respondentky dělají, by se daly shrnout do těchto bodů:

\begin{itemize}
	\item co nejméně na sebe upozorňovat,
	\item informovat své okolí na parametry cesty (otázky odkud, kudy, kam, kdy),
	\item pravidelně někoho kontaktovat,
	\item dávat pozor, co se v okolí děje, a co nejméně se nechat rozptylovat.
\end{itemize}

\subsubsection{Sociální sítě a internet}

Jedním z míst, kde se téma bezpečí velmi často diskutuje, jsou sociální sítě. Ačkoliv to není plně věrohodný zdroj, může dokreslit obrázek o situaci.

Zaujal mě příspěvek na sociální síti Twitter\cite{twitter}, který bych zde rád uvedl: \uv{\emph{Ženy: RT (retweet, poznámka autora) pokud jste někdy šly přes parkoviště s~klíči mezi prsty nebo jste předstíraly, že telefonujete, protože jste se necítily bezpečně.}}\cite{kaitlin_curtice_tweet} (přeloženo autorem) Tento příspěvek měl dne 6. 3. 2022 skoro 77 tisíc oblíbení a 64 tisíc retweetů (přesdílení) a mnoho uživatelů pod ním psalo obdobně znějící komentáře, že drží klíče mezi prsty, vyhýbá se určitým místům, předstírá hovory či jsou stále s někým v kontaktu.

\subsection{Shrnutí}

Během analýzy cílové skupiny jsem si potvrdil, že téma bezpečnosti cestování je velmi relevantním a že velká část lidí se i v dnešní době necítí bezpečně. Dále jsem si v kvalitativním výzkumu přiblížil základní činnosti, které během cest oslovené respondentky dělají a získal i nápady na funkcionality, které by jim jejich situace ulehčily či zpříjemnily. Samotné funkcionality jsou rozebrány až v kapitole \ref{chap:functional_requirements} Funkční požadavky.
