\settocdepth{section}

\chapter{Metodiky vývoje}

Metodiky jsou standardizací pracovních procesů a přinášejí zjednodušení řízení projektu. V rámci vývoje softwaru jich existuje celá řada a liší se svým přístupem k organizaci, psaní samotného kódu či dokumentace. Jejich hlavním přínosem je přehlednost, opakovatelnost, snadnější kontrola a odhadování. Mezi sebou mají celou řadu odlišností, a proto je nutné zvažovat výběr vhodné metodiky na základě velikosti týmu, typu vyvíjeného produktu či pouze na subjektivních pocitech členů vývojové skupiny, při kterém přístupu jsou nejvíce efektivní.

Mezi nejznámější patří:

\begin{itemize}
	\item Waterfall
	\item Iterativní
	\item Prototyping
	\item Spiral
	\item Rapid application development (RAD)
	\item SCRUM
	\item Extrémní programování
\end{itemize}

\section{Klasické vs agilní metodiky}

Metodiky vývoje lze dělit na dvě základní skupiny –~klasické a agilní.

\textbf{Klasické metodiky} vývoje kladou větší důraz na tvorbu a udržování dokumentace. Dbají na počáteční přípravu a analýzu. Bývají tedy propracovanější, s čímž ovšem narůstá prvotní složitost, avšak tato složitost je investicí do budoucna při dalších úpravách a vylepšeních či začlenění nového kolegy.

\clearpage
\textbf{Agilní metodiky} se naopak snaží eliminovat nadbytečné procesy a nástroje, preferují spolupráci se zákazníkem, fungující software nad rozsáhlou dokumentací či reakci na změnu namísto držení se předem definovaného plánu.

Samotné metodiky nemusí striktně dodržovat jeden z těchto dvou přístupů, avšak popis tohoto dělení slouží k uvědomění si, jakými směry se mohou vydávat.

%\section{Klasické metodiky}

\section{Waterfall\cite{waterfall_model}}

Vodopádový model se skládá z několika fází, kdy před začátkem jedné musí být předešlé plně dokončené. Klade důraz na analytickou a plánovací část –~vytváření dokumentace, diagramů, analýzu požadavků.

Jednotlivé fáze se podle různých zdrojů liší, avšak princip zůstává prakticky stejný. Já jsem je vizualizoval na obrázku \ref{fig:waterfall} a shrnul v následujícím seznamu:

\begin{enumerate}
	\item počáteční sběr informací a požadavků,
	\item analýza,
	\item návrh,
	\item implementace,
	\item nasazení a testování,
	\item údržba.
\end{enumerate}

\textbf{Výhodou} vodopádového modelu je jeho jednoduchost, důsledná kontrola výstupů a zpracování podpůrné dokumentace. \textbf{Nevýhodou} pak absence reagování na změny a nemožnost průběžného testování.

\begin{figure}[h!]
    \centering
	\includegraphics[width=1\textwidth]{images/waterfall.png}
   	\caption{Waterfall (vodopád)}
	\label{fig:waterfall}
\end{figure}

\section{Iterativní a inkrementální\cite{iterative_model, incremental_model}}

Iterativní a inkrementální způsoby vývoje rozšiřují vodopádový model a zvyšují jeho použitelnost v praktickém světě. Vývoj se rozdělí na několik menších částí, u kterých se následně pokračuje již známým sekvenčním přístupem. Iterativní a inkrementální přístupy se liší v několika detailech:

\begin{itemize}
	\item \textbf{Iterativní} nelpí na dokončení určité části, ale zpětná vazba může přicházet přímo při vývoji konkrétní funkcionality, která se tak vypiluje.
	\item \textbf{Inkrementální} se zaměřuje na vyvinutí nutného funkčního minima a~až je potřeba, tak se vyvíjí další funkcionality (obrázek \ref{fig:incremental}).
\end{itemize}

\textbf{Výhodou} iterativního a inkrementálního přístupu oproti klasickému vodopádu je jeho větší flexibilita a možnost reakce na změny a nově zjištěné skutečnosti během vývoje. Tento model navíc umožňuje paralelní vývoj pro komplexnější systémy, u čehož je však nutné zkorigování, aby nedošlo k nejasnostem v rámci následujícího propojení.

\begin{figure}[h!]
    \centering
	\includegraphics[width=1\textwidth]{images/incremental.png}
   	\caption{Inkrementální metodika}
	\label{fig:incremental}
\end{figure}

\section{Prototyping\cite{prototyping_model}}

Prototyping model je iterativní proces, při kterém se vytváří funkční prototyp systému, který nabízí v malém měřítku kopii koncového produktu. Tento prototyp se využívá k zjištění zpětné vazby zákazníka, jenž sám často předem neví přesné požadavky. Iterativním přístupem se společnými silami tým dostane k~finální specifikaci produktu s možností využít již hotové naimplementované části do finálního systému. Vizuální přiblížení lze pozorovat na obrázku \ref{fig:prototyping}.

\textbf{Výhodou} prototypingu je aktivní účast zákazníka, která zvyšuje jeho spokojenost s finálním produktem, velká flexibilita ve zpracování změn a nových požadavků a včasné odchycení problémů při testování. 

Naopak \textbf{nevýhodou} jsou velké náklady, kdy se celý proces může protáhnout díky zapojení zákazníka, nedůsledně vedená dokumentace a pro samotné vývojáře může nastat chaos v zadávání požadavků.

\begin{figure}[h!]
    \centering
	\includegraphics[width=1\textwidth]{images/prototyping.png}
   	\caption{Prototyping}
	\label{fig:prototyping}
\end{figure}

\section{Spiral\cite{spiral_model}}

Spirální model je iterativní model soustřeďující se na analýzu rizik. V grafické reprezentaci (obrázek \ref{fig:spiral}) znázorňuje spirálu rozdělenou na čtyři kvadranty. Každá smyčka reprezentuje jednu fázi sestávající právě ze čtyř částí, kterými jsou:

\begin{enumerate}
	\item sběr požadavků, jejich analýza a nalezení alternativních cest,
	\item identifikace a analýza rizik,
	\item vývoj nové verze produktu a testování,
	\item zhodnocení zákazníkem a plánování další fáze.
\end{enumerate}

\textbf{Výhodou} tohoto modelu je jeho zaměření na rizika, která z velké části nelze předem odhadnout. Z iterativního charakteru je zde opět flexibilita v reagování na změny, a zákazník tak může být plně spokojen.

Hlavní \textbf{nevýhodou} tohoto modelu je jeho komplexita, jež se nehodí na menší projekty. Samotná analýza rizik je náročná jak časově, tak často i finančně.

\begin{figure}[h!]
    \centering
	\includegraphics[width=1\textwidth]{images/spiral.png}
   	\caption{Spiral}
	\label{fig:spiral}
\end{figure}

\section{Rapid application development (RAD)\cite{rapid_model, prototyping_vs_rad}}

Rapid application development je modelem kladoucím důraz na rychlost vývoje za použití iterativního přístupu. Rozděluje celý projekt na moduly, které lze vyvíjet paralelně, a využívá nástrojů pro automatické generování kódu. Skládá se ze čtyř hlavních fází, kde fáze 2 a 3 se iterativně opakují:

\begin{enumerate}
	\item \textbf{Requirements planning} – obnáší analýzu úkolů, tvoření scénářů, případů užití, brainstorming.
	\item \textbf{User description} – během této fáze se získává uživatelova zpětná vazba, prototyp se upravuje, aby splňoval jejich potřeby.
	\item \textbf{Construction} – samotný vývoj, kterého uživatel může být stále součástí a navrhovat změny, použití automatizovaných nástrojů.
	\item \textbf{Cutover} – testování, integrace a přijetí.
\end{enumerate}

\textbf{Výhodou} je rychlost vývoje, důsledkem čehož klesá i samotná cena. Model včasně reaguje na změny a nová zjištění. \textbf{Nevýhodou} je složitější organizace a synchronizace týmů při paralelním vývoji a rychlost vývoje se může projevit na kvalitě kódu.

\begin{figure}[h!]
    \centering
	\includegraphics[width=1\textwidth]{images/rapid.jpg}
   	\caption{Rapid application development (RAD)\cite{rapid_jpg}}
	\label{fig:rapid}
\end{figure}

\section{SCRUM\cite{scrum_model, agile_models, agile_bi_si}}

Scrum je agilní metodikou využívající iterativní proces, jehož základem jsou sprinty trvající určitou dobu, většinou v rozmezí jednoho týdne až měsíce. Priorita je na týmu jako celku, který se může sám organizovat. Pro jeho pochopení je nutné vysvětlit pár pojmů a vizualizace je na obrázku \ref{fig:scrum}:

\begin{itemize}
	\item \textbf{Product backlog} je seznam všech požadavků, jež se od výsledného systému očekávají.
	\item \textbf{Srum master} kontroluje dodržování celého procesu.
	\item \textbf{Product owner} odpovídá za chod projektu a prioritizuje požadavky v~product backlogu.
	\item \textbf{Sprint} je iterací, na jejímž konci je nová verze produktu.
	\item \textbf{Sprint backlog} je seznam požadavků pro jeden konkrétní sprint.
\end{itemize}

Samotný sprint má následující části:

\begin{itemize}
	\item \textbf{Sprint planning} je počáteční plánovací schůzka, během které se upřesňuje sprint backlog.
	\item \textbf{Scrum} je jednodenní iterací uvnitř sprintu začínající krátkou úvodní schůzkou, kde by se měl celý tým sesynchronizovat.
	\item \textbf{Sprint review} je konečné zhodnocení výstupů sprintu a jejich prezentování.
	\item \textbf{Sprint retrospective} je zhodnocení samotného fungování sprintu a~hledání možností, jak zefektivnit a zkvalitnit práci.
\end{itemize}

\textbf{Výhodou} přístupu Scrum je jeho důraz na zákazníkovu spokojenost, pořádek ve vedení a sesynchronizování týmu, který následně může být mnohem efektivnější. Změny v požadavcích nemohou být provedeny v rámci začatého sprintu, což je nevýhodou pro zákazníka, avšak samotný tým nemá následně v~práci chaos. Scrum popisuje spíš organizaci, nežli samotný vývoj a je vhodné ho zkombinovat například s praktikami z extrémního programování.

\begin{figure}[h!]
    \centering
	\includegraphics[width=1\textwidth]{images/scrum.png}
   	\caption{Scrum\cite{scrum_png}}
	\label{fig:scrum}
\end{figure}

\section{Extrémní programování\cite{xp_model}}

Extrémní programování je agilní metodikou, která si klade za cíl vzít osvědčené postupy a posunout je na extrémní úroveň. Příklady takových principů jsou:

\begin{itemize}
	\item \textbf{Párové programování a kontrola} – extrémní programování doporučuje párové programování, kdy jeden na druhého dohlíží a po určitém časovém intervalu se střídají. Také pomocí kontroly kódu lze odhalit velké množství problémů.
	\item \textbf{Testování} – časté testování, kdy mnohdy jsou samotné testy psané dříve než funkcionalita.
	\item \textbf{Integrační testování} – psaná funkcionalita by se měla testovat pravidelně v rámci celého systému a to klidně několikrát denně.
	\item \textbf{Komunikace} – velmi důležitá je komunikace týmu se zákazníkem tak, aby byl výsledně maximálně spokojen.
	\item \textbf{Jednoduchost} – funkcionality se píšou až v momentě, kdy jsou potřeba, požadavky se rychle mění a práce navíc je zbytečná.
	\item \textbf{Odvaha} – odvaha zahodit stávající části implementace či dělat velká rozhodnutí.
\end{itemize}

\textbf{Výhodou} extrémního programování je jeho důraz na kvalitu a funkčnost, nová funkcionalita je otestována a nerozbíjí stávající, komunikace se zákazníkem předchází nedorozuměním, nevzniká technický dluh do budoucna, avšak mnohdy je nedostatečná dokumentace. \textbf{Nevýhodou} je náročnost, kterou s~sebou přináší častá komunikace se zákazníkem a obecně častá komunikace celého týmu a dodatečná práce spojená s testováním a párovým programováním, což může protahovat celý vývoj.

\section{Test driven development\cite{test_driven_model}}

Programování řízené testy je způsob, v jehož prvním kroku po specifikování funkcionalit není jejich programování, nýbrž napsání testů, které ověřují jejich funkčnost. Až následně se píše samotný kód, jenž je při nesplnění testů upravován.

\section{Shrnutí a výběr pro vývoj aplikace Safie}

V této kapitole jsem popsal nejznámější metodiky vývoje a uvedl jejich výhody a nevýhody. V praktickém použití se nutně nevylučují a není nezbytné se držet jejich přesných hranic. 

Pro výběr metodiky na vývoj aplikace v rámci této diplomové práce jsem bral v potaz důležitou skutečnost, že systém vyvíjím sám, z čehož mi vyšlo několik bodů:

\begin{itemize}
	\item Není potřeba pořádat schůzky a synchronizovat se v týmu.
	\item Ačkoliv se dokumentace bude hodit do budoucna, není nutné na ni klást přílišný důraz na úkor samotného vývoje, neboť se vyznám v jednotlivých částech systému a tedy je lehčí se vyvarovat problémům při jejich propojení.
	\item V rámci tvorby zastávám roli vývojového týmu i zákazníka, tudíž je zde absence nutnosti vytváření prototypů a získávání zpětné vazby od klienta.
\end{itemize}

Z těchto důvodů jsem se rozhodl vývoj vést iterativním přístupem –~tedy vyvinout jednu konkrétní funkcionalitou a až následně se věnovat další. 